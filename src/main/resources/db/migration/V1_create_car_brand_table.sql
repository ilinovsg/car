create table car_brand (
    brand_id int4 primary key,
    brand_name varchar not null
);

create table car_model (
    model_id int4 primary key,
    model_name varchar not null,
    brand_id int4
);
create table item (
    item_id int8 primary key,
    title varchar not null,
    item_link varchar not null,
    image_link varchar not null,
    price int not null,
    saved_time timestamp not null
);
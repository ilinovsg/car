package jse77.sample.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import jse77.sample.model.CarBrand;

public interface CarBrandRepository extends JpaRepository<CarBrand, Integer> {
}

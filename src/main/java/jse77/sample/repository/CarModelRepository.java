package jse77.sample.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import jse77.sample.model.CarModel;

public interface CarModelRepository extends JpaRepository<CarModel, Integer> {
    List<CarModel> findAllByBrandId(Integer brandId);
}

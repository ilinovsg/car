package jse77.sample.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import java.io.Serializable;

import lombok.Data;

@Data
@Entity
@Table(name = "car_model")
public class CarModel implements Serializable {
    @Id
    @Column(name = "model_id")
    private Integer id;

    @Column(name = "model_name")
    private String name;

    @Column(name = "brand_id")
    private Integer brandId;
}

package jse77.sample.service;

import jse77.sample.repository.CarBrandRepository;
import lombok.extern.slf4j.Slf4j;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.json.simple.parser.ParseException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@Service
public class CarService {

    private final File file = new File(
            getClass().getClassLoader().getResource("host.properties").getFile()
    );
    private static List<String> listOfUrls = new ArrayList<>();
    private static URL urlModels;

    private final CarBrandRepository carBrandRepository;

    @Autowired
    public CarService(CarBrandRepository carBrandRepository) {
        this.carBrandRepository = carBrandRepository;
    }

    public void proceedSearchCars() throws Exception {

        String hosts = inputStreamToString(new FileInputStream(file));

        URL urlBrands = new URL("http://"+hosts+"/brands/");
        urlModels = new URL("http://"+hosts+"/models/");

        HttpClient httpClient = HttpClient.newHttpClient();
        HttpRequest httpRequest = HttpRequest.newBuilder().uri(new URI(urlBrands.toString())).GET().build();
        try {
            HttpResponse<String> httpResponse = httpClient.send(httpRequest, HttpResponse.BodyHandlers.ofString());
            if (httpResponse.statusCode() == 200) {
                parseCurrentJson(httpResponse.body());
            }
        } catch (IOException | InterruptedException e) {

        }
    }

    public static String parseUrl(URL url) {
        if (url == null) {
            return "";
        }
        StringBuilder stringBuilder = new StringBuilder();
        try (BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()))) {
            String inputLine;
            while ((inputLine = in.readLine()) != null) {
                stringBuilder.append(inputLine);
            }
        } catch (IOException e) {

        }
        return stringBuilder.toString();
    }

    public static void parseCurrentJson(String resultJson) {
        try {
            JSONArray carJsonObject = (JSONArray) JSONValue.parseWithException(resultJson);

            for(int n = 0; n < carJsonObject.size(); n++)
            {
                JSONObject object = (JSONObject) carJsonObject.get(n);
                System.out.println("Бренд: " + object.get("id") + object.get("name"));

                URL url = new URL(urlModels.toString()+object.get("id")+"/");
                String resultModelJson = parseUrl(url);

                parseChildJson(resultModelJson);
            }

        } catch (ParseException | MalformedURLException e) {
            e.printStackTrace();
        }
    }

    public static void parseChildJson(String resultJson) {
        try {
            JSONArray carJsonObject = (JSONArray) JSONValue.parseWithException(resultJson);

            for(int n = 0; n < carJsonObject.size(); n++)
            {
                JSONObject object = (JSONObject) carJsonObject.get(n);
                System.out.println("Марка: " + object.get("id") +", "+ object.get("name") +", "+ object.get("brandId"));
            }

        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    public static String inputStreamToString(FileInputStream is) throws IOException {
        StringBuilder sb = new StringBuilder();
        String line;
        BufferedReader br = new BufferedReader(new InputStreamReader(is));
        while ((line = br.readLine()) != null) {
            sb.append(line);
            listOfUrls.add(line);
        }
        br.close();
        return sb.toString();
    }
}

package jse77.sample.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

@Service
public class SearchService {

    private final CarService carService;

    @Autowired
    public SearchService(CarService carService) {
        this.carService = carService;
    }

    @Scheduled(fixedRate = 10000)
    public void proceeding() throws Exception {
        carService.proceedSearchCars();
    }
}
